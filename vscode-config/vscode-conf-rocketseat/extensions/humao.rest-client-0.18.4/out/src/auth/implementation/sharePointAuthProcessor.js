"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const workspaceUtility_1 = require("./../../workspaceUtility");
const node_sp_auth_config_1 = require("node-sp-auth-config");
const spauth = require("node-sp-auth");
const path = require("path");
const url = require("url");
class SharePointAuthProcessor {
    constructor(request, authPath) {
        this.request = request;
        this.authPath = authPath;
    }
    process(options) {
        return __awaiter(this, void 0, void 0, function* () {
            let workspacePath = workspaceUtility_1.getWorkspaceRootPath();
            let configPath;
            if (path.isAbsolute(this.authPath)) {
                configPath = this.authPath;
            }
            else {
                configPath = path.join(workspacePath, this.authPath);
            }
            const authConfig = new node_sp_auth_config_1.AuthConfig({
                configPath: configPath
            });
            let ctx = yield authConfig.getContext();
            let requestUrl = this.request.url;
            let siteUrlParts = url.parse(ctx.siteUrl);
            let siteUrlRoot = `${siteUrlParts.protocol}//${siteUrlParts.host}`;
            // return original options if request url doesn't match sharepoint url from config file
            if (requestUrl.indexOf(siteUrlRoot) === -1) {
                return options;
            }
            let auth = yield spauth.getAuth(ctx.siteUrl, ctx.authOptions);
            let headers = Object.assign(options.headers, auth.headers);
            options = Object.assign(options, auth.options);
            options.headers = headers;
            return options;
        });
    }
}
exports.SharePointAuthProcessor = SharePointAuthProcessor;
//# sourceMappingURL=sharePointAuthProcessor.js.map