"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const Constants = require("../constants");
const sharePointAuthProcessor_1 = require("./implementation/sharePointAuthProcessor");
const passThroughProcessor_1 = require("./implementation/passThroughProcessor");
const environmentController_1 = require("../controllers/environmentController");
class AuthProcessorFactory {
    static resolveAuthProcessor(request) {
        return __awaiter(this, void 0, void 0, function* () {
            let environmentVariables = yield environmentController_1.EnvironmentController.getCustomVariables();
            let sharepointAuthPath = environmentVariables.get(Constants.SharePointAuthEnvironmentKey);
            if (sharepointAuthPath) {
                return new sharePointAuthProcessor_1.SharePointAuthProcessor(request, sharepointAuthPath);
            }
            return new passThroughProcessor_1.PassThroughProcessor();
        });
    }
}
exports.AuthProcessorFactory = AuthProcessorFactory;
//# sourceMappingURL=authProcessorFactory.js.map