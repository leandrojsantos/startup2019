'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const fs = __importStar(require("fs-extra"));
const os = __importStar(require("os"));
const path = __importStar(require("path"));
const Constants = __importStar(require("../common/constants"));
class PersistUtility {
    static async saveRequest(httpRequest) {
        let requests = await PersistUtility.deserializeFromFileAsync(PersistUtility.historyFilePath, PersistUtility.emptyHttpRequestItems);
        requests.unshift(httpRequest);
        requests = requests.slice(0, Constants.HistoryItemsMaxCount);
        await fs.writeJson(PersistUtility.historyFilePath, requests);
    }
    static loadRequests() {
        return PersistUtility.deserializeFromFileAsync(PersistUtility.historyFilePath, PersistUtility.emptyHttpRequestItems);
    }
    static clearRequests() {
        return fs.writeJson(PersistUtility.historyFilePath, PersistUtility.emptyHttpRequestItems);
    }
    static async saveEnvironment(environment) {
        await PersistUtility.ensureFileAsync(PersistUtility.environmentFilePath);
        await fs.writeJson(PersistUtility.environmentFilePath, environment);
    }
    static loadEnvironment() {
        return PersistUtility.deserializeFromFileAsync(PersistUtility.environmentFilePath);
    }
    static ensureCookieFile() {
        fs.ensureFileSync(PersistUtility.cookieFilePath);
    }
    static ensureFileAsync(path) {
        return fs.ensureFile(path);
    }
    static async deserializeFromFileAsync(path, defaultValue) {
        try {
            return await fs.readJson(path);
        }
        catch (_a) {
            await fs.ensureFile(path);
            return defaultValue;
        }
    }
}
exports.PersistUtility = PersistUtility;
PersistUtility.historyFilePath = path.join(os.homedir(), Constants.ExtensionFolderName, Constants.HistoryFileName);
PersistUtility.cookieFilePath = path.join(os.homedir(), Constants.ExtensionFolderName, Constants.CookieFileName);
PersistUtility.environmentFilePath = path.join(os.homedir(), Constants.ExtensionFolderName, Constants.EnvironmentFileName);
PersistUtility.emptyHttpRequestItems = [];
//# sourceMappingURL=persistUtility.js.map