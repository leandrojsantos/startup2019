"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const configurationSettings_1 = require("../models/configurationSettings");
class ConditionalRegistration {
    constructor(doRegister) {
        this.doRegister = doRegister;
    }
    dispose() {
        if (this.registration) {
            this.registration.dispose();
            this.registration = undefined;
        }
    }
    update(enabled) {
        if (enabled) {
            if (!this.registration) {
                this.registration = this.doRegister();
            }
        }
        else {
            if (this.registration) {
                this.registration.dispose();
                this.registration = undefined;
            }
        }
    }
}
class ConfigurationDependentRegistration {
    constructor(register, settingValueFunc) {
        this.settingValueFunc = settingValueFunc;
        this.settings = configurationSettings_1.RestClientSettings.Instance;
        this.registration = new ConditionalRegistration(register);
        this.update();
        this.settings.onDidChangeConfiguration(this.update, this);
    }
    dispose() {
        this.registration.dispose();
    }
    update() {
        this.registration.update(this.settingValueFunc(this.settings));
    }
}
exports.ConfigurationDependentRegistration = ConfigurationDependentRegistration;
//# sourceMappingURL=dependentRegistration.js.map