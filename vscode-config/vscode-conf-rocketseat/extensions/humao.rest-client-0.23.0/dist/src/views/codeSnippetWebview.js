'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const dispose_1 = require("../utils/dispose");
const baseWebview_1 = require("./baseWebview");
const hljs = require('highlight.js');
const codeHighlightLinenums = require('code-highlight-linenums');
class CodeSnippetWebview extends baseWebview_1.BaseWebview {
    constructor() {
        super();
        this.codeSnippetPreviewActiveContextKey = 'codeSnippetPreviewFocus';
    }
    get viewType() {
        return 'rest-code-snippet';
    }
    async render(convertResult, title, lang) {
        let panel;
        if (this.panels.length === 0) {
            panel = vscode_1.window.createWebviewPanel(this.viewType, title, vscode_1.ViewColumn.Two, {
                enableFindWidget: true,
                retainContextWhenHidden: true,
                localResourceRoots: [this.styleFolderPath]
            });
            panel.onDidDispose(() => {
                vscode_1.commands.executeCommand('setContext', this.codeSnippetPreviewActiveContextKey, false);
                this.panels.pop();
                this._onDidCloseAllWebviewPanels.fire();
            });
            panel.onDidChangeViewState(({ webviewPanel }) => {
                vscode_1.commands.executeCommand('setContext', this.codeSnippetPreviewActiveContextKey, webviewPanel.visible);
            });
            this.panels.push(panel);
        }
        else {
            panel = this.panels[0];
            panel.title = title;
        }
        panel.webview.html = this.getHtmlForWebview(panel, convertResult, lang);
        vscode_1.commands.executeCommand('setContext', this.codeSnippetPreviewActiveContextKey, true);
        panel.reveal(vscode_1.ViewColumn.Two);
    }
    dispose() {
        dispose_1.disposeAll(this.panels);
    }
    getHtmlForWebview(panel, convertResult, lang) {
        const csp = this.getCsp();
        return `
            <head>
                <link rel="stylesheet" href="${panel.webview.asWebviewUri(this.styleFilePath)}">
                ${csp}
            </head>
            <body>
                <div>
                    <pre><code>${codeHighlightLinenums(convertResult, { hljs, lang: this.getHighlightJsLanguageAlias(lang), start: 1 })}</code></pre>
                    <a id="scroll-to-top" role="button" aria-label="scroll to top" onclick="window.scroll(0,0)"><span class="icon"></span></a>
                </div>
            </body>`;
    }
    getHighlightJsLanguageAlias(lang) {
        if (!lang || lang === 'shell') {
            return 'bash';
        }
        if (lang === 'node') {
            return 'javascript';
        }
        return lang;
    }
    getCsp() {
        return `<meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src 'self' http: https: data: vscode-resource:; style-src 'self' 'unsafe-inline' http: https: data: vscode-resource:;">`;
    }
}
exports.CodeSnippetWebview = CodeSnippetWebview;
//# sourceMappingURL=codeSnippetWebview.js.map