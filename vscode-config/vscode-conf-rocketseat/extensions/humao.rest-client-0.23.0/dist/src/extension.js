'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
const vscode_1 = require("vscode");
const codeSnippetController_1 = require("./controllers/codeSnippetController");
const environmentController_1 = require("./controllers/environmentController");
const historyController_1 = require("./controllers/historyController");
const requestController_1 = require("./controllers/requestController");
const responseController_1 = require("./controllers/responseController");
const logger_1 = require("./logger");
const customVariableDiagnosticsProvider_1 = require("./providers/customVariableDiagnosticsProvider");
const documentLinkProvider_1 = require("./providers/documentLinkProvider");
const environmentOrFileVariableHoverProvider_1 = require("./providers/environmentOrFileVariableHoverProvider");
const fileVariableDefinitionProvider_1 = require("./providers/fileVariableDefinitionProvider");
const fileVariableReferenceProvider_1 = require("./providers/fileVariableReferenceProvider");
const fileVariableReferencesCodeLensProvider_1 = require("./providers/fileVariableReferencesCodeLensProvider");
const httpCodeLensProvider_1 = require("./providers/httpCodeLensProvider");
const httpCompletionItemProvider_1 = require("./providers/httpCompletionItemProvider");
const httpDocumentSymbolProvider_1 = require("./providers/httpDocumentSymbolProvider");
const requestVariableCompletionItemProvider_1 = require("./providers/requestVariableCompletionItemProvider");
const requestVariableDefinitionProvider_1 = require("./providers/requestVariableDefinitionProvider");
const requestVariableHoverProvider_1 = require("./providers/requestVariableHoverProvider");
const aadTokenCache_1 = require("./utils/aadTokenCache");
const dependentRegistration_1 = require("./utils/dependentRegistration");
// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
async function activate(context) {
    const logger = new logger_1.Logger();
    const requestController = new requestController_1.RequestController(context, logger);
    const historyController = new historyController_1.HistoryController(logger);
    const responseController = new responseController_1.ResponseController();
    const codeSnippetController = new codeSnippetController_1.CodeSnippetController();
    const environmentController = new environmentController_1.EnvironmentController(await environmentController_1.EnvironmentController.getCurrentEnvironment());
    context.subscriptions.push(requestController);
    context.subscriptions.push(historyController);
    context.subscriptions.push(responseController);
    context.subscriptions.push(codeSnippetController);
    context.subscriptions.push(environmentController);
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.request', ((document, range) => requestController.run(range))));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.rerun-last-request', () => requestController.rerun()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.cancel-request', () => requestController.cancel()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.history', () => historyController.save()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.clear-history', () => historyController.clear()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.save-response', () => responseController.save()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.save-response-body', () => responseController.saveBody()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.copy-response-body', () => responseController.copyBody()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.generate-codesnippet', () => codeSnippetController.run()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.copy-codesnippet', () => codeSnippetController.copy()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.copy-request-as-curl', () => codeSnippetController.copyAsCurl()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.switch-environment', () => environmentController.switchEnvironment()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.clear-aad-token-cache', () => aadTokenCache_1.AadTokenCache.clear()));
    context.subscriptions.push(vscode_1.commands.registerCommand('rest-client._openDocumentLink', args => {
        vscode_1.workspace.openTextDocument(vscode_1.Uri.parse(args.path)).then(vscode_1.window.showTextDocument, error => {
            vscode_1.window.showErrorMessage(error.message);
        });
    }));
    const documentSelector = [
        { language: 'http', scheme: 'file' },
        { language: 'http', scheme: 'untitled' },
    ];
    context.subscriptions.push(vscode_1.languages.registerCompletionItemProvider(documentSelector, new httpCompletionItemProvider_1.HttpCompletionItemProvider()));
    context.subscriptions.push(vscode_1.languages.registerCompletionItemProvider(documentSelector, new requestVariableCompletionItemProvider_1.RequestVariableCompletionItemProvider(), '.'));
    context.subscriptions.push(vscode_1.languages.registerHoverProvider(documentSelector, new environmentOrFileVariableHoverProvider_1.EnvironmentOrFileVariableHoverProvider()));
    context.subscriptions.push(vscode_1.languages.registerHoverProvider(documentSelector, new requestVariableHoverProvider_1.RequestVariableHoverProvider()));
    context.subscriptions.push(new dependentRegistration_1.ConfigurationDependentRegistration(() => vscode_1.languages.registerCodeLensProvider(documentSelector, new httpCodeLensProvider_1.HttpCodeLensProvider()), s => s.enableSendRequestCodeLens));
    context.subscriptions.push(new dependentRegistration_1.ConfigurationDependentRegistration(() => vscode_1.languages.registerCodeLensProvider(documentSelector, new fileVariableReferencesCodeLensProvider_1.FileVariableReferencesCodeLensProvider()), s => s.enableCustomVariableReferencesCodeLens));
    context.subscriptions.push(vscode_1.languages.registerDocumentLinkProvider(documentSelector, new documentLinkProvider_1.RequestBodyDocumentLinkProvider()));
    context.subscriptions.push(vscode_1.languages.registerDefinitionProvider(documentSelector, new fileVariableDefinitionProvider_1.FileVariableDefinitionProvider()));
    context.subscriptions.push(vscode_1.languages.registerDefinitionProvider(documentSelector, new requestVariableDefinitionProvider_1.RequestVariableDefinitionProvider()));
    context.subscriptions.push(vscode_1.languages.registerReferenceProvider(documentSelector, new fileVariableReferenceProvider_1.FileVariableReferenceProvider()));
    context.subscriptions.push(vscode_1.languages.registerDocumentSymbolProvider(documentSelector, new httpDocumentSymbolProvider_1.HttpDocumentSymbolProvider()));
    const diagnosticsProviders = new customVariableDiagnosticsProvider_1.CustomVariableDiagnosticsProvider();
    vscode_1.workspace.onDidOpenTextDocument(diagnosticsProviders.checkVariables, diagnosticsProviders, context.subscriptions);
    vscode_1.workspace.onDidCloseTextDocument(diagnosticsProviders.deleteDocumentFromDiagnosticCollection, diagnosticsProviders, context.subscriptions);
    vscode_1.workspace.onDidSaveTextDocument(diagnosticsProviders.checkVariables, diagnosticsProviders, context.subscriptions);
}
exports.activate = activate;
// this method is called when your extension is deactivated
function deactivate() {
}
exports.deactivate = deactivate;
//# sourceMappingURL=extension.js.map