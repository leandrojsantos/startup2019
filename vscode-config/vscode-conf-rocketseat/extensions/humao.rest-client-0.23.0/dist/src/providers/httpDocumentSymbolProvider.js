'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const os_1 = require("os");
const url = __importStar(require("url"));
const vscode_1 = require("vscode");
const Constants = __importStar(require("../common/constants"));
const requestParserFactory_1 = require("../models/requestParserFactory");
const selector_1 = require("../utils/selector");
const variableProcessor_1 = require("../utils/variableProcessor");
const workspaceUtility_1 = require("../utils/workspaceUtility");
class HttpDocumentSymbolProvider {
    async provideDocumentSymbols(document, token) {
        const symbols = [];
        const lines = document.getText().split(Constants.LineSplitterRegex);
        const requestRange = selector_1.Selector.getRequestRanges(lines, { ignoreFileVariableDefinitionLine: false });
        for (let [blockStart, blockEnd] of requestRange) {
            // get real start for current requestRange
            while (blockStart <= blockEnd) {
                const line = lines[blockStart];
                if (selector_1.Selector.isEmptyLine(line) ||
                    selector_1.Selector.isCommentLine(line)) {
                    blockStart++;
                }
                else if (selector_1.Selector.isVariableDefinitionLine(line)) {
                    const [name, container] = this.getVariableSymbolInfo(line);
                    symbols.push(new vscode_1.SymbolInformation(name, vscode_1.SymbolKind.Variable, container, new vscode_1.Location(document.uri, new vscode_1.Range(blockStart, 0, blockStart, line.length))));
                    blockStart++;
                }
                else {
                    break;
                }
            }
            if (selector_1.Selector.isResponseStatusLine(lines[blockStart])) {
                continue;
            }
            if (blockStart <= blockEnd) {
                const text = await variableProcessor_1.VariableProcessor.processRawRequest(lines.slice(blockStart, blockEnd + 1).join(os_1.EOL));
                const info = this.getRequestSymbolInfo(text);
                if (!info) {
                    continue;
                }
                const [name, container] = info;
                symbols.push(new vscode_1.SymbolInformation(name, vscode_1.SymbolKind.Method, container, new vscode_1.Location(document.uri, new vscode_1.Range(blockStart, 0, blockEnd, lines[blockEnd].length))));
            }
        }
        return symbols;
    }
    getVariableSymbolInfo(line) {
        const fileName = workspaceUtility_1.getCurrentHttpFileName();
        line = line.trim();
        return [line.substring(1, line.indexOf('=')).trim(), fileName];
    }
    getRequestSymbolInfo(text) {
        const parser = HttpDocumentSymbolProvider.requestParserFactory.createRequestParser(text);
        const request = parser.parseHttpRequest(text, vscode_1.window.activeTextEditor.document.fileName);
        if (!request) {
            return null;
        }
        const parsedUrl = url.parse(request.url);
        return [`${request.method} ${parsedUrl.path}`, parsedUrl.host || ''];
    }
}
exports.HttpDocumentSymbolProvider = HttpDocumentSymbolProvider;
HttpDocumentSymbolProvider.requestParserFactory = new requestParserFactory_1.RequestParserFactory();
//# sourceMappingURL=httpDocumentSymbolProvider.js.map