'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const Constants = __importStar(require("../common/constants"));
const variableUtility_1 = require("../utils/variableUtility");
class FileVariableDefinitionProvider {
    async provideDefinition(document, position, token) {
        if (!variableUtility_1.VariableUtility.isEnvironmentOrFileVariableReference(document, position)) {
            return undefined;
        }
        const documentLines = document.getText().split(Constants.LineSplitterRegex);
        const wordRange = document.getWordRangeAtPosition(position);
        const selectedVariableName = document.getText(wordRange);
        const locations = variableUtility_1.VariableUtility.getFileVariableDefinitionRanges(documentLines, selectedVariableName);
        return locations.map(location => new vscode_1.Location(document.uri, location));
    }
}
exports.FileVariableDefinitionProvider = FileVariableDefinitionProvider;
//# sourceMappingURL=fileVariableDefinitionProvider.js.map