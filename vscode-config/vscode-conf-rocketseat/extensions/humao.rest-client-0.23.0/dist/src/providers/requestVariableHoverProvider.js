'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const requestVariableProvider_1 = require("../utils/httpVariableProviders/requestVariableProvider");
const variableUtility_1 = require("../utils/variableUtility");
class RequestVariableHoverProvider {
    async provideHover(document, position, token) {
        if (!variableUtility_1.VariableUtility.isRequestVariableReference(document, position)) {
            return undefined;
        }
        const wordRange = document.getWordRangeAtPosition(position, /\{\{(\w+)\.(.*?)?\}\}/);
        const lineRange = document.lineAt(position);
        const fullPath = this.getRequestVariableHoverPath(wordRange, lineRange);
        if (!fullPath) {
            return undefined;
        }
        const { name, value, warning, error } = await requestVariableProvider_1.RequestVariableProvider.Instance.get(fullPath, document);
        if (!error && !warning) {
            const contents = [];
            if (value) {
                contents.push(typeof value === 'string' ? value : { language: 'json', value: JSON.stringify(value, null, 2) });
            }
            contents.push(new vscode_1.MarkdownString(`*Request Variable* \`${name}\``));
            return new vscode_1.Hover(contents, wordRange);
        }
        return undefined;
    }
    getRequestVariableHoverPath(wordRange, lineRange) {
        return wordRange && !wordRange.isEmpty
            ? lineRange.text.substring(wordRange.start.character + 2, wordRange.end.character - 2)
            : null;
    }
}
exports.RequestVariableHoverProvider = RequestVariableHoverProvider;
//# sourceMappingURL=requestVariableHoverProvider.js.map