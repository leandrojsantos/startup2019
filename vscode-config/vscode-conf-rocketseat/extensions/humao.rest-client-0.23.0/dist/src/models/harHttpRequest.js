"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const url_1 = require("url");
class HARHeader {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}
exports.HARHeader = HARHeader;
class HARCookie {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}
exports.HARCookie = HARCookie;
class HARParam {
    constructor(name, value) {
        this.name = name;
        this.value = value;
    }
}
exports.HARParam = HARParam;
class HARPostData {
    constructor(mimeType, text) {
        this.mimeType = mimeType;
        this.text = text;
        if (mimeType === 'application/x-www-form-urlencoded') {
            if (text) {
                text = decodeURIComponent(text.replace(/\+/g, '%20'));
                this.params = [];
                const pairs = text.split('&');
                pairs.forEach(pair => {
                    const [key, ...values] = pair.split('=');
                    this.params.push(new HARParam(key, values.join('=')));
                });
            }
        }
    }
}
exports.HARPostData = HARPostData;
class HARHttpRequest {
    constructor(method, url, headers, cookies, postData) {
        this.method = method;
        this.url = url;
        this.headers = headers;
        this.cookies = cookies;
        this.postData = postData;
        const queryObj = url_1.parse(url, true).query;
        this.queryString = this.flatten(queryObj);
    }
    flatten(queryObj) {
        const queryParams = [];
        Object.keys(queryObj).forEach(name => {
            const value = queryObj[name];
            if (Array.isArray(value)) {
                queryParams.push(...value.map(v => new HARParam(name, v)));
            }
            else {
                queryParams.push(new HARParam(name, value));
            }
        });
        return queryParams;
    }
}
exports.HARHttpRequest = HARHttpRequest;
//# sourceMappingURL=harHttpRequest.js.map