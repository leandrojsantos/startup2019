'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const configurationSettings_1 = require("./models/configurationSettings");
const logLevel_1 = require("./models/logLevel");
let outputChannel;
function getOrCreateOutputChannel() {
    if (!outputChannel) {
        outputChannel = vscode_1.window.createOutputChannel('REST');
    }
    return outputChannel;
}
class Logger {
    constructor() {
        this._restClientSettings = configurationSettings_1.RestClientSettings.Instance;
        this._outputChannel = getOrCreateOutputChannel();
    }
    verbose(message, data) {
        this.log(logLevel_1.LogLevel.Verbose, message, data);
    }
    info(message, data) {
        this.log(logLevel_1.LogLevel.Info, message, data);
    }
    warn(message, data) {
        this.log(logLevel_1.LogLevel.Warn, message, data);
    }
    error(message, data) {
        this.log(logLevel_1.LogLevel.Error, message, data);
    }
    log(level, message, data) {
        if (level >= this._restClientSettings.logLevel) {
            this._outputChannel.appendLine(`[${logLevel_1.LogLevel[level]}  - ${(new Date().toLocaleTimeString())}] ${message}`);
            if (data) {
                this._outputChannel.appendLine(this.data2String(data));
            }
        }
    }
    data2String(data) {
        if (data instanceof Error) {
            return data.stack || data.message;
        }
        if (typeof data === 'string') {
            return data;
        }
        return JSON.stringify(data, null, 2);
    }
}
exports.Logger = Logger;
//# sourceMappingURL=logger.js.map