'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const Constants = __importStar(require("../common/constants"));
const variableUtility_1 = require("../utils/variableUtility");
class FileVariableReferenceProvider {
    async provideReferences(document, position, context, token) {
        if (!variableUtility_1.VariableUtility.isFileVariableDefinition(document, position) && !variableUtility_1.VariableUtility.isEnvironmentOrFileVariableReference(document, position)) {
            return undefined;
        }
        const documentLines = document.getText().split(Constants.LineSplitterRegex);
        const wordRange = document.getWordRangeAtPosition(position);
        const selectedVariableName = document.getText(wordRange);
        const locations = [];
        if (context.includeDeclaration) {
            const definitionLocations = variableUtility_1.VariableUtility.getFileVariableDefinitionRanges(documentLines, selectedVariableName);
            locations.push(...definitionLocations);
        }
        const referenceLocations = variableUtility_1.VariableUtility.getFileVariableReferenceRanges(documentLines, selectedVariableName);
        locations.push(...referenceLocations);
        return locations.map(location => new vscode_1.Location(document.uri, location));
    }
}
exports.FileVariableReferenceProvider = FileVariableReferenceProvider;
//# sourceMappingURL=fileVariableReferenceProvider.js.map