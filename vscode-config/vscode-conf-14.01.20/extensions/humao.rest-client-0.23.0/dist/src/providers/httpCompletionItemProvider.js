'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const httpElement_1 = require("../models/httpElement");
const httpElementFactory_1 = require("../utils/httpElementFactory");
const variableUtility_1 = require("../utils/variableUtility");
class HttpCompletionItemProvider {
    async provideCompletionItems(document, position, token) {
        if (variableUtility_1.VariableUtility.isPartialRequestVariableReference(document, position)) {
            return undefined;
        }
        const elements = await httpElementFactory_1.HttpElementFactory.getHttpElements(document, document.lineAt(position).text);
        return elements.map(e => {
            const item = new vscode_1.CompletionItem(e.name);
            item.detail = `HTTP ${httpElement_1.ElementType[e.type]}`;
            item.documentation = e.description;
            item.insertText = e.text;
            item.kind = e.type in [httpElement_1.ElementType.SystemVariable, httpElement_1.ElementType.EnvironmentCustomVariable, httpElement_1.ElementType.FileCustomVariable, httpElement_1.ElementType.RequestCustomVariable]
                ? vscode_1.CompletionItemKind.Variable
                : e.type === httpElement_1.ElementType.Method
                    ? vscode_1.CompletionItemKind.Method
                    : e.type === httpElement_1.ElementType.Header
                        ? vscode_1.CompletionItemKind.Property
                        : vscode_1.CompletionItemKind.Field;
            return item;
        });
    }
}
exports.HttpCompletionItemProvider = HttpCompletionItemProvider;
//# sourceMappingURL=httpCompletionItemProvider.js.map