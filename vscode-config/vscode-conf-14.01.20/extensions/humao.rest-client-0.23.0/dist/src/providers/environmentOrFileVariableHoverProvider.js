'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const environmentVariableProvider_1 = require("../utils/httpVariableProviders/environmentVariableProvider");
const fileVariableProvider_1 = require("../utils/httpVariableProviders/fileVariableProvider");
const variableUtility_1 = require("../utils/variableUtility");
class EnvironmentOrFileVariableHoverProvider {
    async provideHover(document, position, token) {
        if (!variableUtility_1.VariableUtility.isEnvironmentOrFileVariableReference(document, position)) {
            return undefined;
        }
        const wordRange = document.getWordRangeAtPosition(position);
        const selectedVariableName = document.getText(wordRange);
        if (await fileVariableProvider_1.FileVariableProvider.Instance.has(selectedVariableName, document)) {
            const { name, value, error, warning } = await fileVariableProvider_1.FileVariableProvider.Instance.get(selectedVariableName, document);
            if (!warning && !error) {
                const contents = [value, new vscode_1.MarkdownString(`*File Variable* \`${name}\``)];
                return new vscode_1.Hover(contents, wordRange);
            }
            return undefined;
        }
        if (await environmentVariableProvider_1.EnvironmentVariableProvider.Instance.has(selectedVariableName)) {
            const { name, value, error, warning } = await environmentVariableProvider_1.EnvironmentVariableProvider.Instance.get(selectedVariableName);
            if (!warning && !error) {
                const contents = [value, new vscode_1.MarkdownString(`*Environment Variable* \`${name}\``)];
                return new vscode_1.Hover(contents, wordRange);
            }
        }
        return undefined;
    }
}
exports.EnvironmentOrFileVariableHoverProvider = EnvironmentOrFileVariableHoverProvider;
//# sourceMappingURL=environmentOrFileVariableHoverProvider.js.map