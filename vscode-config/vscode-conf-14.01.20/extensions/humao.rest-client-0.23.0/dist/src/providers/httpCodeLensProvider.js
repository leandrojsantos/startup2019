'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const Constants = __importStar(require("../common/constants"));
const selector_1 = require("../utils/selector");
class HttpCodeLensProvider {
    provideCodeLenses(document, token) {
        const blocks = [];
        const lines = document.getText().split(Constants.LineSplitterRegex);
        const requestRanges = selector_1.Selector.getRequestRanges(lines);
        for (const [blockStart, blockEnd] of requestRanges) {
            const range = new vscode_1.Range(blockStart, 0, blockEnd, 0);
            const cmd = {
                arguments: [document, range],
                title: 'Send Request',
                command: 'rest-client.request'
            };
            blocks.push(new vscode_1.CodeLens(range, cmd));
        }
        return Promise.resolve(blocks);
    }
}
exports.HttpCodeLensProvider = HttpCodeLensProvider;
//# sourceMappingURL=httpCodeLensProvider.js.map