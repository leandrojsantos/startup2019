"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class EnvironmentPickItem {
    constructor(label, name, description) {
        this.label = label;
        this.name = name;
        this.description = description;
    }
}
exports.EnvironmentPickItem = EnvironmentPickItem;
//# sourceMappingURL=environmentPickItem.js.map