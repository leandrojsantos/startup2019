'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
var LogLevel;
(function (LogLevel) {
    LogLevel[LogLevel["Verbose"] = 0] = "Verbose";
    LogLevel[LogLevel["Info"] = 1] = "Info";
    LogLevel[LogLevel["Warn"] = 2] = "Warn";
    LogLevel[LogLevel["Error"] = 3] = "Error";
})(LogLevel = exports.LogLevel || (exports.LogLevel = {}));
function fromString(value) {
    value = value.toLowerCase();
    switch (value) {
        case 'verbose':
            return LogLevel.Verbose;
        case 'info':
            return LogLevel.Info;
        case 'warn':
            return LogLevel.Warn;
        case 'error':
        default:
            return LogLevel.Error;
    }
}
exports.fromString = fromString;
//# sourceMappingURL=logLevel.js.map