'use strict';
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const path = __importStar(require("path"));
const vscode_1 = require("vscode");
const Constants = __importStar(require("../common/constants"));
const previewOption_1 = require("../models/previewOption");
const decorator_1 = require("../utils/decorator");
const dispose_1 = require("../utils/dispose");
const mimeUtility_1 = require("../utils/mimeUtility");
const misc_1 = require("../utils/misc");
const responseFormatUtility_1 = require("../utils/responseFormatUtility");
const baseWebview_1 = require("./baseWebview");
const hljs = require('highlight.js');
class HttpResponseWebview extends baseWebview_1.BaseWebview {
    constructor(context) {
        super();
        this.context = context;
        this.urlRegex = /(https?:\/\/[^\s"'<>\]\)]+)/gi;
        this.httpResponsePreviewActiveContextKey = 'httpResponsePreviewFocus';
        // Init response webview map
        this.panelResponses = new Map();
        this.iconFilePath = vscode_1.Uri.file(path.join(this.extensionPath, Constants.ImagesFolderName, Constants.IconFileName));
        this.context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.fold-response', () => this.foldResponseBody()));
        this.context.subscriptions.push(vscode_1.commands.registerCommand('rest-client.unfold-response', () => this.unfoldResponseBody()));
    }
    get viewType() {
        return 'rest-response';
    }
    async render(response, column) {
        const tabTitle = this.settings.requestNameAsResponseTabTitle && response.request.requestVariableCacheKey && response.request.requestVariableCacheKey.key || 'Response';
        let panel;
        if (this.settings.showResponseInDifferentTab || this.panels.length === 0) {
            panel = vscode_1.window.createWebviewPanel(this.viewType, `${tabTitle}(${response.elapsedMillionSeconds}ms)`, { viewColumn: column, preserveFocus: !this.settings.previewResponsePanelTakeFocus }, {
                enableFindWidget: true,
                enableScripts: true,
                retainContextWhenHidden: true,
                localResourceRoots: [this.styleFolderPath, this.scriptFolderPath]
            });
            panel.onDidDispose(() => {
                const response = this.panelResponses.get(panel);
                if (response === HttpResponseWebview.activePreviewResponse) {
                    vscode_1.commands.executeCommand('setContext', this.httpResponsePreviewActiveContextKey, false);
                    this.activePanel = undefined;
                    HttpResponseWebview.activePreviewResponse = undefined;
                }
                const index = this.panels.findIndex(v => v === panel);
                if (index !== -1) {
                    this.panels.splice(index, 1);
                    this.panelResponses.delete(panel);
                }
                if (this.panels.length === 0) {
                    this._onDidCloseAllWebviewPanels.fire();
                }
            });
            panel.iconPath = this.iconFilePath;
            panel.onDidChangeViewState(({ webviewPanel }) => {
                vscode_1.commands.executeCommand('setContext', this.httpResponsePreviewActiveContextKey, webviewPanel.active);
                this.activePanel = webviewPanel.active ? webviewPanel : undefined;
                HttpResponseWebview.activePreviewResponse = webviewPanel.active ? this.panelResponses.get(webviewPanel) : undefined;
            });
            this.panels.push(panel);
        }
        else {
            panel = this.panels[this.panels.length - 1];
            panel.title = `${tabTitle}(${response.elapsedMillionSeconds}ms)`;
        }
        panel.webview.html = this.getHtmlForWebview(panel, response);
        vscode_1.commands.executeCommand('setContext', this.httpResponsePreviewActiveContextKey, this.settings.previewResponsePanelTakeFocus);
        panel.reveal(column, !this.settings.previewResponsePanelTakeFocus);
        this.panelResponses.set(panel, response);
        this.activePanel = panel;
        HttpResponseWebview.activePreviewResponse = response;
    }
    dispose() {
        dispose_1.disposeAll(this.panels);
    }
    foldResponseBody() {
        if (this.activePanel) {
            this.activePanel.webview.postMessage({ 'command': 'foldAll' });
        }
    }
    unfoldResponseBody() {
        if (this.activePanel) {
            this.activePanel.webview.postMessage({ 'command': 'unfoldAll' });
        }
    }
    getHtmlForWebview(panel, response) {
        let innerHtml;
        let width = 2;
        let contentType = response.contentType;
        if (contentType) {
            contentType = contentType.trim();
        }
        if (contentType && mimeUtility_1.MimeUtility.isBrowserSupportedImageFormat(contentType) && !HttpResponseWebview.isHeadRequest(response)) {
            innerHtml = `<img src="data:${contentType};base64,${response.bodyBuffer.toString('base64')}">`;
        }
        else {
            const code = this.highlightResponse(response);
            width = (code.split(/\r\n|\r|\n/).length + 1).toString().length;
            innerHtml = `<pre><code>${this.addLineNums(code)}</code></pre>`;
        }
        // Content Security Policy
        const nonce = new Date().getTime() + '' + new Date().getMilliseconds();
        const csp = this.getCsp(nonce);
        return `
    <head>
        <link rel="stylesheet" type="text/css" href="${panel.webview.asWebviewUri(this.styleFilePath)}">
        ${this.getSettingsOverrideStyles(width)}
        ${csp}
        <script nonce="${nonce}">
            document.addEventListener('DOMContentLoaded', function () {
                document.getElementById('scroll-to-top')
                        .addEventListener('click', function () { window.scrollTo(0,0); });
            });
        </script>
    </head>
    <body>
        <div>
            ${this.settings.disableAddingHrefLinkForLargeResponse && response.bodySizeInBytes > this.settings.largeResponseBodySizeLimitInMB * 1024 * 1024
            ? innerHtml
            : this.addUrlLinks(innerHtml)}
            <a id="scroll-to-top" role="button" aria-label="scroll to top" title="Scroll To Top"><span class="icon"></span></a>
        </div>
        <script type="text/javascript" src="${panel.webview.asWebviewUri(this.scriptFilePath)}" nonce="${nonce}" charset="UTF-8"></script>
    </body>`;
    }
    highlightResponse(response) {
        let code = '';
        const previewOption = this.settings.previewOption;
        if (previewOption === previewOption_1.PreviewOption.Exchange) {
            // for add request details
            const request = response.request;
            const requestNonBodyPart = `${request.method} ${request.url} HTTP/1.1
${HttpResponseWebview.formatHeaders(request.headers)}`;
            code += hljs.highlight('http', requestNonBodyPart + '\r\n').value;
            if (request.body) {
                if (typeof request.body !== 'string') {
                    request.body = 'NOTE: Request Body From File Is Not Shown';
                }
                const requestBodyPart = `${responseFormatUtility_1.ResponseFormatUtility.formatBody(request.body, request.contentType, true)}`;
                const bodyLanguageAlias = HttpResponseWebview.getHighlightLanguageAlias(request.contentType, request.body);
                if (bodyLanguageAlias) {
                    code += hljs.highlight(bodyLanguageAlias, requestBodyPart).value;
                }
                else {
                    code += hljs.highlightAuto(requestBodyPart).value;
                }
                code += '\r\n';
            }
            code += '\r\n'.repeat(2);
        }
        if (previewOption !== previewOption_1.PreviewOption.Body) {
            const responseNonBodyPart = `HTTP/${response.httpVersion} ${response.statusCode} ${response.statusMessage}
${HttpResponseWebview.formatHeaders(response.headers)}`;
            code += hljs.highlight('http', responseNonBodyPart + (previewOption !== previewOption_1.PreviewOption.Headers ? '\r\n' : '')).value;
        }
        if (previewOption !== previewOption_1.PreviewOption.Headers) {
            const responseBodyPart = `${responseFormatUtility_1.ResponseFormatUtility.formatBody(response.body, response.contentType, this.settings.suppressResponseBodyContentTypeValidationWarning)}`;
            if (this.settings.disableHighlightResonseBodyForLargeResponse &&
                response.bodySizeInBytes > this.settings.largeResponseBodySizeLimitInMB * 1024 * 1024) {
                code += responseBodyPart;
            }
            else {
                const bodyLanguageAlias = HttpResponseWebview.getHighlightLanguageAlias(response.contentType, responseBodyPart);
                if (bodyLanguageAlias) {
                    code += hljs.highlight(bodyLanguageAlias, responseBodyPart).value;
                }
                else {
                    code += hljs.highlightAuto(responseBodyPart).value;
                }
            }
        }
        return code;
    }
    getSettingsOverrideStyles(width) {
        return [
            '<style>',
            (this.settings.fontFamily || this.settings.fontSize || this.settings.fontWeight ? [
                'code {',
                this.settings.fontFamily ? `font-family: ${this.settings.fontFamily};` : '',
                this.settings.fontSize ? `font-size: ${this.settings.fontSize}px;` : '',
                this.settings.fontWeight ? `font-weight: ${this.settings.fontWeight};` : '',
                '}',
            ] : []).join('\n'),
            'code .line {',
            `padding-left: calc(${width}ch + 20px );`,
            '}',
            'code .line:before {',
            `width: ${width}ch;`,
            `margin-left: calc(-${width}ch + -30px );`,
            '}',
            '.line .icon {',
            `left: calc(${width}ch + 3px)`,
            '}',
            '.line.collapsed .icon {',
            `left: calc(${width}ch + 3px)`,
            '}',
            '</style>'
        ].join('\n');
    }
    getCsp(nonce) {
        return `<meta http-equiv="Content-Security-Policy" content="default-src 'none'; img-src 'self' http: https: data: vscode-resource:; script-src 'nonce-${nonce}'; style-src 'self' 'unsafe-inline' http: https: data: vscode-resource:;">`;
    }
    addLineNums(code) {
        code = code.replace(/([\r\n]\s*)(<\/span>)/ig, '$2$1');
        code = this.cleanLineBreaks(code);
        code = code.split(/\r\n|\r|\n/);
        const max = (1 + code.length).toString().length;
        const foldingRanges = this.getFoldingRange(code);
        code = code
            .map(function (line, i) {
            const lineNum = i + 1;
            const range = foldingRanges.has(lineNum)
                ? ` range-start="${foldingRanges.get(lineNum).start}" range-end="${foldingRanges.get(lineNum).end}"`
                : '';
            const folding = foldingRanges.has(lineNum) ? '<span class="icon"></span>' : '';
            return `<span class="line width-${max}" start="${lineNum}"${range}>${line}${folding}</span>`;
        })
            .join('\n');
        return code;
    }
    cleanLineBreaks(code) {
        const openSpans = [], matcher = /<\/?span[^>]*>|\r\n|\r|\n/ig, newline = /\r\n|\r|\n/, closingTag = /^<\//;
        return code.replace(matcher, function (match) {
            if (newline.test(match)) {
                if (openSpans.length) {
                    return openSpans.map(() => '</span>').join('') + match + openSpans.join('');
                }
                else {
                    return match;
                }
            }
            else if (closingTag.test(match)) {
                openSpans.pop();
                return match;
            }
            else {
                openSpans.push(match);
                return match;
            }
        });
    }
    addUrlLinks(innerHtml) {
        return innerHtml.replace(this.urlRegex, '<a href="$1" target="_blank" rel="noopener noreferrer">$1</a>');
    }
    getFoldingRange(lines) {
        const result = new Map();
        const stack = [];
        const leadingSpaceCount = lines
            .map((line, index) => [index, line.search(/\S/)])
            .filter(([, num]) => num !== -1);
        for (const [index, [lineIndex, count]] of leadingSpaceCount.entries()) {
            if (index === 0) {
                continue;
            }
            const [prevLineIndex, prevCount] = leadingSpaceCount[index - 1];
            if (prevCount < count) {
                stack.push([prevLineIndex, prevCount]);
            }
            else if (prevCount > count) {
                let prev;
                while ((prev = stack.slice(-1)[0]) && (prev[1] >= count)) {
                    stack.pop();
                    result.set(prev[0] + 1, new FoldingRange(prev[0] + 1, lineIndex));
                }
            }
        }
        return result;
    }
    static formatHeaders(headers) {
        let headerString = '';
        for (const header in headers) {
            if (headers.hasOwnProperty(header)) {
                let value = headers[header];
                if (typeof headers[header] !== 'string') {
                    value = headers[header];
                }
                headerString += `${header}: ${value}\n`;
            }
        }
        return headerString;
    }
    static getHighlightLanguageAlias(contentType, content = null) {
        if (mimeUtility_1.MimeUtility.isJSON(contentType)) {
            return 'json';
        }
        else if (mimeUtility_1.MimeUtility.isJavaScript(contentType)) {
            return 'javascript';
        }
        else if (mimeUtility_1.MimeUtility.isXml(contentType)) {
            return 'xml';
        }
        else if (mimeUtility_1.MimeUtility.isHtml(contentType)) {
            return 'html';
        }
        else if (mimeUtility_1.MimeUtility.isCSS(contentType)) {
            return 'css';
        }
        else {
            // If content is provided, guess from content if not content type is matched
            if (content) {
                if (misc_1.isJSONString(content)) {
                    return 'json';
                }
            }
            return null;
        }
    }
    static isHeadRequest({ request: { method } }) {
        return !!(method && method.toLowerCase() === 'head');
    }
}
__decorate([
    decorator_1.trace('Fold Response')
], HttpResponseWebview.prototype, "foldResponseBody", null);
__decorate([
    decorator_1.trace('Unfold Response')
], HttpResponseWebview.prototype, "unfoldResponseBody", null);
exports.HttpResponseWebview = HttpResponseWebview;
class FoldingRange {
    constructor(start, end) {
        this.start = start;
        this.end = end;
    }
}
//# sourceMappingURL=httpResponseWebview.js.map