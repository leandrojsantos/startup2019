'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
const variableType_1 = require("../models/variableType");
const environmentVariableProvider_1 = require("./httpVariableProviders/environmentVariableProvider");
const fileVariableProvider_1 = require("./httpVariableProviders/fileVariableProvider");
const requestVariableProvider_1 = require("./httpVariableProviders/requestVariableProvider");
const systemVariableProvider_1 = require("./httpVariableProviders/systemVariableProvider");
const workspaceUtility_1 = require("./workspaceUtility");
class VariableProcessor {
    static async processRawRequest(request) {
        const variableReferenceRegex = /\{{2}(.+?)\}{2}/g;
        let result = '';
        let match;
        let lastIndex = 0;
        const resolvedVariables = new Map();
        variable: while (match = variableReferenceRegex.exec(request)) {
            result += request.substring(lastIndex, match.index);
            lastIndex = variableReferenceRegex.lastIndex;
            const name = match[1].trim();
            const document = workspaceUtility_1.getCurrentTextDocument();
            const context = { rawRequest: request, parsedRequest: result };
            for (const [provider, cacheable] of VariableProcessor.providers) {
                if (resolvedVariables.has(name)) {
                    result += resolvedVariables.get(name);
                    continue variable;
                }
                if (await provider.has(name, document, context)) {
                    const { value, error, warning } = await provider.get(name, document, context);
                    if (!error && !warning) {
                        if (cacheable) {
                            resolvedVariables.set(name, value);
                        }
                        result += value;
                        continue variable;
                    }
                    else {
                        break;
                    }
                }
            }
            result += `{{${name}}}`;
        }
        result += request.substring(lastIndex);
        return result;
    }
    static async getAllVariablesDefinitions(document) {
        const [, [requestProvider], [fileProvider], [environmentProvider]] = VariableProcessor.providers;
        const requestVariables = await requestProvider.getAll(document);
        const fileVariables = await fileProvider.getAll(document);
        const environmentVariables = await environmentProvider.getAll();
        const variableDefinitions = new Map();
        // Request variables in file
        requestVariables.forEach(({ name }) => {
            if (variableDefinitions.has(name)) {
                variableDefinitions.get(name).push(variableType_1.VariableType.Request);
            }
            else {
                variableDefinitions.set(name, [variableType_1.VariableType.Request]);
            }
        });
        // Normal file variables
        fileVariables.forEach(({ name }) => {
            if (variableDefinitions.has(name)) {
                variableDefinitions.get(name).push(variableType_1.VariableType.File);
            }
            else {
                variableDefinitions.set(name, [variableType_1.VariableType.File]);
            }
        });
        // Environment variables
        environmentVariables.forEach(({ name }) => {
            if (variableDefinitions.has(name)) {
                variableDefinitions.get(name).push(variableType_1.VariableType.Environment);
            }
            else {
                variableDefinitions.set(name, [variableType_1.VariableType.Environment]);
            }
        });
        return variableDefinitions;
    }
}
exports.VariableProcessor = VariableProcessor;
VariableProcessor.providers = [
    [systemVariableProvider_1.SystemVariableProvider.Instance, false],
    [requestVariableProvider_1.RequestVariableProvider.Instance, true],
    [fileVariableProvider_1.FileVariableProvider.Instance, true],
    [environmentVariableProvider_1.EnvironmentVariableProvider.Instance, true],
];
//# sourceMappingURL=variableProcessor.js.map