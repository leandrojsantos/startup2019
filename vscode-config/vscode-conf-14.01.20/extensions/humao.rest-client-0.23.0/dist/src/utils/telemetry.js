'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const appInsights = __importStar(require("applicationinsights"));
const package_lock_json_1 = __importDefault(require("../../package-lock.json"));
const package_json_1 = __importDefault(require("../../package.json"));
const Constants = __importStar(require("../common/constants"));
const configurationSettings_1 = require("../models/configurationSettings");
appInsights.setup(Constants.AiKey)
    .setAutoCollectConsole(false)
    .setAutoCollectDependencies(false)
    .setAutoCollectExceptions(false)
    .setAutoCollectPerformance(false)
    .setAutoCollectRequests(false)
    .setAutoDependencyCorrelation(false)
    .setUseDiskRetryCaching(true)
    .start();
class Telemetry {
    static initialize() {
        Telemetry.defaultClient = appInsights.defaultClient;
        const context = Telemetry.defaultClient.context;
        context.tags[context.keys.applicationVersion] = package_json_1.default.version;
        context.tags[context.keys.internalSdkVersion] = `node:${package_lock_json_1.default.dependencies.applicationinsights.version}`;
    }
    static sendEvent(eventName, properties) {
        try {
            if (Telemetry.restClientSettings.enableTelemetry) {
                Telemetry.defaultClient.trackEvent({ name: eventName, properties });
            }
        }
        catch (_a) {
        }
    }
}
exports.Telemetry = Telemetry;
Telemetry.restClientSettings = configurationSettings_1.RestClientSettings.Instance;
Telemetry.initialize();
//# sourceMappingURL=telemetry.js.map