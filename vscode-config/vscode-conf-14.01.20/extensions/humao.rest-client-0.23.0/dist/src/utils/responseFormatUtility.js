'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const jsonc_parser_1 = require("jsonc-parser");
const os = __importStar(require("os"));
const vscode_1 = require("vscode");
const mimeUtility_1 = require("./mimeUtility");
const misc_1 = require("./misc");
const pd = require('pretty-data').pd;
class ResponseFormatUtility {
    static formatBody(body, contentType, suppressValidation) {
        if (contentType) {
            if (mimeUtility_1.MimeUtility.isJSON(contentType)) {
                if (misc_1.isJSONString(body)) {
                    body = ResponseFormatUtility.jsonPrettify(body);
                }
                else if (!suppressValidation) {
                    vscode_1.window.showWarningMessage('The content type of response is application/json, while response body is not a valid json string');
                }
            }
            else if (mimeUtility_1.MimeUtility.isXml(contentType)) {
                body = pd.xml(body);
            }
            else if (mimeUtility_1.MimeUtility.isCSS(contentType)) {
                body = pd.css(body);
            }
            else {
                // Add this for the case that the content type of response body is not very accurate #239
                if (misc_1.isJSONString(body)) {
                    body = ResponseFormatUtility.jsonPrettify(body);
                }
            }
        }
        return body;
    }
    static jsonPrettify(text, indentSize = 2) {
        const scanner = jsonc_parser_1.createScanner(text, true);
        let indentLevel = 0;
        function newLineAndIndent() {
            return os.EOL + ' '.repeat(indentLevel * indentSize);
        }
        function scanNext() {
            const token = scanner.scan();
            const offset = scanner.getTokenOffset();
            const length = scanner.getTokenLength();
            const value = text.substr(offset, length);
            return [token, value];
        }
        let [firstToken, firstTokenValue] = scanNext();
        let secondToken;
        let secondTokenValue;
        let result = '';
        while (firstToken !== 17 /* EOF */) {
            [secondToken, secondTokenValue] = scanNext();
            switch (firstToken) {
                case 1 /* OpenBraceToken */:
                    result += ResponseFormatUtility.jsonSpecialTokenMapping[firstToken];
                    if (secondToken !== 2 /* CloseBraceToken */) {
                        indentLevel++;
                        result += newLineAndIndent();
                    }
                    break;
                case 3 /* OpenBracketToken */:
                    result += ResponseFormatUtility.jsonSpecialTokenMapping[firstToken];
                    if (secondToken !== 4 /* CloseBracketToken */) {
                        indentLevel++;
                        result += newLineAndIndent();
                    }
                    break;
                case 2 /* CloseBraceToken */:
                case 4 /* CloseBracketToken */:
                case 7 /* NullKeyword */:
                case 8 /* TrueKeyword */:
                case 9 /* FalseKeyword */:
                    result += ResponseFormatUtility.jsonSpecialTokenMapping[firstToken];
                    if (secondToken === 2 /* CloseBraceToken */
                        || secondToken === 4 /* CloseBracketToken */) {
                        indentLevel--;
                        result += newLineAndIndent();
                    }
                    break;
                case 5 /* CommaToken */:
                    result += ResponseFormatUtility.jsonSpecialTokenMapping[firstToken];
                    if (secondToken === 2 /* CloseBraceToken */
                        || secondToken === 4 /* CloseBracketToken */) {
                        indentLevel--;
                    }
                    result += newLineAndIndent();
                    break;
                case 6 /* ColonToken */:
                    result += ResponseFormatUtility.jsonSpecialTokenMapping[firstToken] + ' ';
                    break;
                case 10 /* StringLiteral */:
                case 11 /* NumericLiteral */:
                case 16 /* Unknown */:
                    result += firstTokenValue;
                    if (secondToken === 2 /* CloseBraceToken */
                        || secondToken === 4 /* CloseBracketToken */) {
                        indentLevel--;
                        result += newLineAndIndent();
                    }
                    break;
                default:
                    result += firstTokenValue;
            }
            firstToken = secondToken;
            firstTokenValue = secondTokenValue;
        }
        return result;
    }
}
exports.ResponseFormatUtility = ResponseFormatUtility;
ResponseFormatUtility.jsonSpecialTokenMapping = {
    [1 /* OpenBraceToken */]: '{',
    [2 /* CloseBraceToken */]: '}',
    [3 /* OpenBracketToken */]: '[',
    [4 /* CloseBracketToken */]: ']',
    [6 /* ColonToken */]: ':',
    [5 /* CommaToken */]: ',',
    [7 /* NullKeyword */]: 'null',
    [8 /* TrueKeyword */]: 'true',
    [9 /* FalseKeyword */]: 'false'
};
//# sourceMappingURL=responseFormatUtility.js.map