'use strict';
Object.defineProperty(exports, "__esModule", { value: true });
class AadTokenCache {
    static get(key) {
        return AadTokenCache.cache.get(key);
    }
    static set(key, value) {
        AadTokenCache.cache.set(key, value);
    }
    static clear() {
        AadTokenCache.cache.clear();
    }
}
exports.AadTokenCache = AadTokenCache;
AadTokenCache.cache = new Map();
//# sourceMappingURL=aadTokenCache.js.map