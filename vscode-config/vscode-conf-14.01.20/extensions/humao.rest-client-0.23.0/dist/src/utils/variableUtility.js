'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const vscode_1 = require("vscode");
const Constants = __importStar(require("../common/constants"));
class VariableUtility {
    static isFileVariableDefinition(document, position) {
        const wordRange = document.getWordRangeAtPosition(position);
        const lineRange = document.lineAt(position);
        if (!wordRange
            || wordRange.start.character < 1
            || lineRange.text[wordRange.start.character - 1] !== '@') {
            // not a custom variable definition syntax
            return false;
        }
        return true;
    }
    static isEnvironmentOrFileVariableReference(document, position) {
        const wordRange = document.getWordRangeAtPosition(position);
        if (wordRange === undefined) {
            return false;
        }
        const lineRange = document.lineAt(position);
        return VariableUtility.isVariableReferenceFromLine(wordRange, lineRange);
    }
    static isRequestVariableReference(document, position) {
        const wordRange = document.getWordRangeAtPosition(position, /\{\{(\w+)\.(response|request)?(\.body(\..*?)?|\.headers(\.[\w-]+)?)?\}\}/);
        return wordRange ? !wordRange.isEmpty : false;
    }
    static isPartialRequestVariableReference(document, position) {
        const wordRange = document.getWordRangeAtPosition(position, /\{\{(\w+)\.(.*?)?\}\}/);
        return wordRange ? !wordRange.isEmpty : false;
    }
    static isVariableReferenceFromLine(wordRange, lineRange) {
        if (!wordRange
            || wordRange.start.character < 2
            || wordRange.end.character > lineRange.range.end.character - 1
            || lineRange.text[wordRange.start.character - 1] !== '{'
            || lineRange.text[wordRange.start.character - 2] !== '{'
            || lineRange.text[wordRange.end.character] !== '}'
            || lineRange.text[wordRange.end.character + 1] !== '}') {
            // not a custom variable reference syntax
            return false;
        }
        return true;
    }
    static getFileVariableDefinitionRanges(lines, variable) {
        const locations = [];
        for (const [index, line] of lines.entries()) {
            let match;
            if ((match = Constants.FileVariableDefinitionRegex.exec(line)) && match[1] === variable) {
                const startPos = line.indexOf(`@${variable}`);
                const endPos = startPos + variable.length + 1;
                locations.push(new vscode_1.Range(index, startPos, index, endPos));
            }
        }
        return locations;
    }
    static getRequestVariableDefinitionRanges(lines, variable) {
        const locations = [];
        for (const [index, line] of lines.entries()) {
            let match;
            if ((match = Constants.RequestVariableDefinitionRegex.exec(line)) && match[1] === variable) {
                const startPos = line.indexOf(`${variable}`);
                const endPos = startPos + variable.length + 1;
                locations.push(new vscode_1.Range(index, startPos, index, endPos));
            }
        }
        return locations;
    }
    static getFileVariableReferenceRanges(lines, variable) {
        const locations = [];
        const regex = new RegExp(`\{\{${variable}\}\}`, 'g');
        for (const [index, line] of lines.entries()) {
            if (Constants.CommentIdentifiersRegex.test(line)) {
                continue;
            }
            regex.lastIndex = 0;
            let match;
            while (match = regex.exec(line)) {
                const startPos = match.index + 2;
                const endPos = startPos + variable.length;
                locations.push(new vscode_1.Range(index, startPos, index, endPos));
                regex.lastIndex = match.index + 1;
            }
        }
        return locations;
    }
}
exports.VariableUtility = VariableUtility;
//# sourceMappingURL=variableUtility.js.map