'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Constants = __importStar(require("../../common/constants"));
const variableType_1 = require("../../models/variableType");
const misc_1 = require("../misc");
const environmentVariableProvider_1 = require("./environmentVariableProvider");
const requestVariableProvider_1 = require("./requestVariableProvider");
const systemVariableProvider_1 = require("./systemVariableProvider");
class FileVariableProvider {
    constructor() {
        this.escapee = new Map([
            ['n', '\n'],
            ['r', '\r'],
            ['t', '\t']
        ]);
        this.innerVariableProviders = [
            systemVariableProvider_1.SystemVariableProvider.Instance,
            requestVariableProvider_1.RequestVariableProvider.Instance,
            environmentVariableProvider_1.EnvironmentVariableProvider.Instance,
        ];
        this.cache = new Map();
        this.fileMD5Hash = new Map();
        this.type = variableType_1.VariableType.File;
    }
    static get Instance() {
        if (!FileVariableProvider._instance) {
            FileVariableProvider._instance = new FileVariableProvider();
        }
        return FileVariableProvider._instance;
    }
    async has(name, document) {
        const variables = await this.getFileVariables(document);
        return variables.some(v => v.name === name);
    }
    async get(name, document) {
        const variables = await this.getFileVariables(document);
        const variable = variables.find(v => v.name === name);
        if (!variable) {
            return { name, error: "File variable does not exist" /* FileVariableNotExist */ };
        }
        else {
            const variableMap = await this.resolveFileVariables(document, variables);
            return { name, value: variableMap.get(name) };
        }
    }
    async getAll(document) {
        const variables = await this.getFileVariables(document);
        const variableMap = await this.resolveFileVariables(document, variables);
        return [...variableMap.entries()].map(([name, value]) => ({ name, value }));
    }
    async getFileVariables(document) {
        const file = document.uri.toString();
        const fileContent = document.getText();
        const fileHash = misc_1.calculateMD5Hash(fileContent);
        if (!this.cache.has(file) || fileHash !== this.fileMD5Hash.get(file)) {
            const regex = new RegExp(Constants.FileVariableDefinitionRegex, 'mg');
            const variables = new Map();
            let match;
            while (match = regex.exec(fileContent)) {
                const [, key, originalValue] = match;
                let value = "";
                let isPrevCharEscape = false;
                for (const currentChar of originalValue) {
                    if (isPrevCharEscape) {
                        isPrevCharEscape = false;
                        value += this.escapee.get(currentChar) || currentChar;
                    }
                    else {
                        if (currentChar === "\\") {
                            isPrevCharEscape = true;
                            continue;
                        }
                        value += currentChar;
                    }
                }
                variables.set(key, { name: key, value });
            }
            this.cache.set(file, [...variables.values()]);
            this.fileMD5Hash.set(file, fileHash);
        }
        return this.cache.get(file);
    }
    async resolveFileVariables(document, variables) {
        // Resolve non-file variables in variable value
        const fileVariableNames = new Set(variables.map(v => v.name));
        const resolvedVariables = await Promise.all(variables.map(async ({ name, value }) => {
            const parsedValue = await this.processNonFileVariableValue(document, value, fileVariableNames);
            return { name, value: parsedValue };
        }));
        const variableMap = new Map(resolvedVariables.map(({ name, value }) => [name, value]));
        const dependentVariables = new Map();
        const dependencyCount = new Map();
        const noDependencyVariables = [];
        for (const [name, value] of variableMap) {
            const dependentVariableNames = new Set(this.resolveDependentFileVariableNames(value).filter(v => variableMap.has(v)));
            if (dependentVariableNames.size === 0) {
                noDependencyVariables.push(name);
            }
            else {
                dependencyCount.set(name, dependentVariableNames.size);
                dependentVariableNames.forEach(dname => {
                    if (dependentVariables.has(dname)) {
                        dependentVariables.get(dname).push(name);
                    }
                    else {
                        dependentVariables.set(dname, [name]);
                    }
                });
            }
        }
        // Resolve all dependent file variables to actual value
        while (noDependencyVariables.length !== 0) {
            const current = noDependencyVariables.shift();
            if (!dependentVariables.has(current)) {
                continue;
            }
            const dependents = dependentVariables.get(current);
            dependents.forEach(d => {
                const originalValue = variableMap.get(d);
                const currentValue = originalValue.replace(new RegExp(`{{\\s*${current}\s*}}`, 'g'), variableMap.get(current));
                variableMap.set(d, currentValue);
                const newCount = dependencyCount.get(d) - 1;
                if (newCount === 0) {
                    noDependencyVariables.push(d);
                    dependencyCount.delete(d);
                }
                else {
                    dependencyCount.set(d, newCount);
                }
            });
        }
        return variableMap;
    }
    async processNonFileVariableValue(document, value, variables) {
        const variableReferenceRegex = /\{{2}(.+?)\}{2}/g;
        let result = '';
        let match;
        let lastIndex = 0;
        variable: while (match = variableReferenceRegex.exec(value)) {
            result += value.substring(lastIndex, match.index);
            lastIndex = variableReferenceRegex.lastIndex;
            const name = match[1].trim();
            if (!variables.has(name)) {
                const context = { rawRequest: value, parsedRequest: result };
                for (const provider of this.innerVariableProviders) {
                    if (await provider.has(name, document, context)) {
                        const { value, error, warning } = await provider.get(name, document, context);
                        if (!error && !warning) {
                            result += value;
                            continue variable;
                        }
                        else {
                            break;
                        }
                    }
                }
            }
            result += `{{${name}}}`;
        }
        result += value.substring(lastIndex);
        return result;
    }
    resolveDependentFileVariableNames(value) {
        const variableReferenceRegex = /\{{2}(.+?)\}{2}/g;
        let match;
        const result = [];
        while (match = variableReferenceRegex.exec(value)) {
            result.push(match[1].trim());
        }
        return result;
    }
}
exports.FileVariableProvider = FileVariableProvider;
//# sourceMappingURL=fileVariableProvider.js.map