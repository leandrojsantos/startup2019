'use strict';
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const Constants = __importStar(require("../../common/constants"));
const environmentController_1 = require("../../controllers/environmentController");
const configurationSettings_1 = require("../../models/configurationSettings");
const variableType_1 = require("../../models/variableType");
class EnvironmentVariableProvider {
    constructor() {
        this._settings = configurationSettings_1.RestClientSettings.Instance;
        this.type = variableType_1.VariableType.Environment;
    }
    static get Instance() {
        if (!EnvironmentVariableProvider._instance) {
            EnvironmentVariableProvider._instance = new EnvironmentVariableProvider();
        }
        return EnvironmentVariableProvider._instance;
    }
    async has(name) {
        const variables = await this.getAvailableVariables();
        return name in variables;
    }
    async get(name) {
        const variables = await this.getAvailableVariables();
        if (!(name in variables)) {
            return { name, error: "Environment variable does not exist" /* EnvironmentVariableNotExist */ };
        }
        return { name, value: variables[name] };
    }
    async getAll() {
        const variables = await this.getAvailableVariables();
        return Object.keys(variables).map(key => ({ name: key, value: variables[key] }));
    }
    async getAvailableVariables() {
        let { name: environmentName } = await environmentController_1.EnvironmentController.getCurrentEnvironment();
        if (environmentName === Constants.NoEnvironmentSelectedName) {
            environmentName = environmentController_1.EnvironmentController.sharedEnvironmentName;
        }
        const variables = this._settings.environmentVariables;
        const currentEnvironmentVariables = variables[environmentName];
        const sharedEnvironmentVariables = variables[environmentController_1.EnvironmentController.sharedEnvironmentName];
        this.mapEnvironmentVariables(currentEnvironmentVariables, sharedEnvironmentVariables);
        return { ...sharedEnvironmentVariables, ...currentEnvironmentVariables };
    }
    mapEnvironmentVariables(current, shared) {
        for (const [key, value] of Object.entries(current)) {
            if (typeof (value) !== "string") {
                continue;
            }
            const variableRegex = /\{{2}\$shared (.+?)\}{2}/;
            const match = variableRegex.exec(value);
            if (!match) {
                continue;
            }
            const referenceKey = match[1].trim();
            if (typeof (shared[referenceKey]) === "string") {
                current[key] = shared[referenceKey];
            }
        }
    }
}
exports.EnvironmentVariableProvider = EnvironmentVariableProvider;
//# sourceMappingURL=environmentVariableProvider.js.map