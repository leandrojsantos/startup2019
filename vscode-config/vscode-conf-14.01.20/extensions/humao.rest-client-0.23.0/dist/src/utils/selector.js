"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const os_1 = require("os");
const Constants = __importStar(require("../common/constants"));
class Selector {
    static getRequestText(editor, range = null) {
        if (!editor || !editor.document) {
            return null;
        }
        let selectedText;
        if (editor.selection.isEmpty || range) {
            const activeLine = !range ? editor.selection.active.line : range.start.line;
            selectedText = Selector.getDelimitedText(editor.document.getText(), activeLine);
        }
        else {
            selectedText = editor.document.getText(editor.selection);
        }
        return selectedText;
    }
    static getRequestRanges(lines, options) {
        options = {
            ignoreCommentLine: true,
            ignoreEmptyLine: true,
            ignoreFileVariableDefinitionLine: true,
            ignoreResponseRange: true,
            ...options
        };
        const requestRanges = [];
        const delimitedLines = Selector.getDelimiterRows(lines);
        delimitedLines.push(lines.length);
        let prev = -1;
        for (const current of delimitedLines) {
            let start = prev + 1;
            let end = current - 1;
            while (start <= end) {
                const startLine = lines[start];
                if (options.ignoreResponseRange && Selector.isResponseStatusLine(startLine)) {
                    break;
                }
                if (options.ignoreCommentLine && Selector.isCommentLine(startLine)
                    || options.ignoreEmptyLine && Selector.isEmptyLine(startLine)
                    || options.ignoreFileVariableDefinitionLine && Selector.isVariableDefinitionLine(startLine)) {
                    start++;
                    continue;
                }
                const endLine = lines[end];
                if (options.ignoreCommentLine && Selector.isCommentLine(endLine)
                    || options.ignoreEmptyLine && Selector.isEmptyLine(endLine)) {
                    end--;
                    continue;
                }
                requestRanges.push([start, end]);
                break;
            }
            prev = current;
        }
        return requestRanges;
    }
    static getRequestVariableDefinitionName(text) {
        const matched = text.match(Constants.RequestVariableDefinitionRegex);
        return matched && matched[1];
    }
    static isCommentLine(line) {
        return Constants.CommentIdentifiersRegex.test(line);
    }
    static isEmptyLine(line) {
        return line.trim() === '';
    }
    static isVariableDefinitionLine(line) {
        return Constants.FileVariableDefinitionRegex.test(line);
    }
    static isResponseStatusLine(line) {
        return Selector.responseStatusLineRegex.test(line);
    }
    static getDelimitedText(fullText, currentLine) {
        const lines = fullText.split(Constants.LineSplitterRegex);
        const delimiterLineNumbers = Selector.getDelimiterRows(lines);
        if (delimiterLineNumbers.length === 0) {
            return fullText;
        }
        // return null if cursor is in delimiter line
        if (delimiterLineNumbers.includes(currentLine)) {
            return null;
        }
        if (currentLine < delimiterLineNumbers[0]) {
            return lines.slice(0, delimiterLineNumbers[0]).join(os_1.EOL);
        }
        if (currentLine > delimiterLineNumbers[delimiterLineNumbers.length - 1]) {
            return lines.slice(delimiterLineNumbers[delimiterLineNumbers.length - 1] + 1).join(os_1.EOL);
        }
        for (let index = 0; index < delimiterLineNumbers.length - 1; index++) {
            const start = delimiterLineNumbers[index];
            const end = delimiterLineNumbers[index + 1];
            if (start < currentLine && currentLine < end) {
                return lines.slice(start + 1, end).join(os_1.EOL);
            }
        }
        return null;
    }
    static getDelimiterRows(lines) {
        const rows = [];
        for (let index = 0; index < lines.length; index++) {
            if (lines[index].match(/^#{3,}/)) {
                rows.push(index);
            }
        }
        return rows;
    }
}
exports.Selector = Selector;
Selector.responseStatusLineRegex = /^\s*HTTP\/[\d.]+/;
//# sourceMappingURL=selector.js.map