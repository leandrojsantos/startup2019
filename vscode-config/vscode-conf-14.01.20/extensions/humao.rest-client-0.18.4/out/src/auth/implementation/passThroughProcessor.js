"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class PassThroughProcessor {
    process(options) {
        return Promise.resolve(options);
    }
}
exports.PassThroughProcessor = PassThroughProcessor;
//# sourceMappingURL=passThroughProcessor.js.map