"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var VariableType;
(function (VariableType) {
    VariableType[VariableType["File"] = 0] = "File";
    VariableType[VariableType["Request"] = 1] = "Request";
    VariableType[VariableType["Global"] = 2] = "Global";
    VariableType[VariableType["Environment"] = 3] = "Environment";
})(VariableType = exports.VariableType || (exports.VariableType = {}));
//# sourceMappingURL=variableType.js.map