"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class RequestLines {
    constructor(range, requestDocumentUri, requestVariable) {
        this.range = range;
        this.requestDocumentUri = requestDocumentUri;
        this.requestVariable = requestVariable;
    }
}
exports.RequestLines = RequestLines;
//# sourceMappingURL=requestLines.js.map