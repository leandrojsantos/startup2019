const routes = require('express').Router()
const multer = require('multer')
const multerConfig = require('../helpers/multer')

const File = require('../db/schemas/fileSchema')

routes.get('/files', async (req, res) => {
    const filesList = await File.find()

    return res.json(filesList)
})

routes.post('/files', multer(multerConfig).single('file'), async (req, res) => {
    //console.log(req.file)
    const {
        originalname: name,
        size,
        filename: key,
        location: url = ''
    } = req.file

    const file = await File.create({
        name,
        size,
        key,
        url
    })

    return res.json(file)
})

routes.delete('/files/:id', async (req, res)=>{
    const fileDelete = await File.findById(req.params.id)

    await fileDelete.remove()
    return res.send()
})

module.exports = routes