const mongoose = require('mongoose')
const fs = require('fs')
const path = require('path')
const {
    promisify
} = require('util')

const FileSchema = new mongoose.Schema({
    name: String,
    size: Number,
    key: String,
    url: String,
    createdAt: {
        type: Date,
        default: Date.now
    }
})

FileSchema.pre('save', function () {
    if (this.NODE_ENV === 'dev') {
        this.url = `${process.env.LOCAL_HOST}/files/${this.key}`
    } else {
        this.url = `${process.env.MONGO_DB}/files/${this.key}`
    }
})

FileSchema.pre('remove', function () {
    if (this.NODE_ENV === 'prod') {
        return res.send()

    } else {
        return promisify(fs.unlink)(path.resolve(__dirname,'..', '..', '..', 'tmp', 'uploads', this.key))

    }
})




module.exports = mongoose.model('File', FileSchema)