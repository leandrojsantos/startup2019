const express = require('express')
const app = express()
const request = require('request-promise-native')

//seta forma de views
app.set('view engine', 'ejs')
app.set('views', './src/views')

//app.use para usar middleware
app.use(express.static('public'))

//rotas
app.get('/photos', async function(req, res){
    const result = await request.get('http://localhost:5000/api/v1/photos')
    //console.log(JSON.parse(result).data)
    const photo = JSON.parse(result).data
    res.render ('photoView', {photo})
})

app.listen(4000, function(){
    console.log('SERVIDOR EXPRESS OK NA PORTA http://localhost:4000/')
    
})