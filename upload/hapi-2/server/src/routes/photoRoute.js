const PhotoHandler = require('../handlers/photoHandler')

module.exports = [{
        method: 'GET',
        path: '/api/v1/photos',
        handler: PhotoHandler.getAll
    },

    {
        method: 'GET',
        path: '/api/v1/photos/{id}',
        handler: PhotoHandler.find
    },

    {
        method: 'POST',
        path: '/api/v1/photos',
        handler: PhotoHandler.save
    },

    {
        method: 'DELETE',
        path: '/api/v1/photos/{id}',
        handler: PhotoHandler.remove,
        options: {
            cors: true
        }
    }

]