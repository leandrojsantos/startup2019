const PhotoRepo = require('../repositories/photoRepo')

//padrao json:api
const transformer = payload => ({
    type: 'photos',
    id: payload.id,
    attributes: {
        nome: payload.nome,
        valor: payload.valor
    },
    links: {
        self: `/api/v1/photos/${payload.id}`
    }
})

//get todos
const getAll = async () => {
    const photos = await PhotoRepo.getAll({})
    return {
        data: photos.map(transformer)
    }

}

//get por id
const find = async (req, h) => {
    const photo = await PhotoRepo.find({
        _id: req.params.id
    })
    return {
        data: (transformer(photo))
    }
}

//post
const save = async (req, h) => {
    const photo = await PhotoRepo.save(req.payload)

    return h.response(transformer(photo)).code(201)
}

//delete
const remove = async (req, h) => {
    await PhotoRepo.remove(req.params.id
    )
    return h.response().code(204)
}

module.exports = {
    getAll,
    save,
    remove,
    find
}