const PhotoModel = require('../models/photoModel')

//get todos
const getAll = async () => await PhotoModel.find({})

//get por id
const find = async id => await PhotoModel.findById(id)

//post
const save = async (payload) => {
    const {
        nome,
        valor
    } = payload

    const photo = new PhotoModel
    photo.nome = nome
    photo.valor = valor

    await photo.save()

    return photo
}

//delete
const remove = async id => await PhotoModel.findByIdAndDelete({
    _id: id
})

module.exports = {
    getAll,
    save,
    remove,
    find
}