const mongoose = require('mongoose')
const Schema = mongoose.Schema

const PhotoSchma = new Schema({
    nome: String,
    valor: Number
})

module.exports = mongoose.model('Photo', PhotoSchma)