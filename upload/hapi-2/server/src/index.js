require('./services/databases/mongodb')

const Hapi = require('@hapi/hapi')
const PhotoRoute = require('./routes/photoRoute')

const init = async () => {

    const server = new Hapi.server({
        port: 5000,
        host: 'localhost'
    });


    server.route(PhotoRoute)

    await server.start()
    console.log('API HAPI START NA PORTA %s', server.info.uri)
};

process.on('unhandledRejection', (err) => {
    console.log(err);
    process.exit(1);
});

init();
