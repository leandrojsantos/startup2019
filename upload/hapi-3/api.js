const {
    join
} = require('path')
const {
    config
} = require('dotenv')

const {
    ok
} = require('assert')

const Hapi = require('hapi')
const env = process.env.NODE_ENV || "dev"
ok(env === "prod" || env === "dev", "environment inválida! Ou prod ou dev")

const configPath = join('./config', `.env.${env}`)

config({
    path: configPath
})

const HapiSwagger = require('hapi-swagger')
const Inert = require('inert')
const Vision = require('vision')


const swaggerConfig = {
    info: {
        title: 'API Restfull MongoDB uplods',
        version: 'v4.0'
    },
}

const Context = require('./src/db/strategies/base/contextStrategy')
const MongoDB = require('./src/db/strategies/mongodb/mongoDbStrategy')

const PhotoSchema = require('./src/db/strategies/mongodb/schemas/photoSchema')

const UtilRoutes = require('./src/routes/utilRoutes')
const PhotoRoutes = require('./src/routes/photoRoutes')


const app = new Hapi.Server({
    port: process.env.PORT,
    routes: {
        cors: true
    }
})


function mapRoutes(instance, methods) {
    return methods.map(method => instance[method]())
}

async function main() {
    const photo = MongoDB.connect()
    const mongoDbFile = new Context(new MongoDB(photo, PhotoSchema))

    await app.register([
        Inert,
        Vision,
        {
            plugin: HapiSwagger,
            options: swaggerConfig
        }
    ])

    app.route([
        //api methods helpers
        ...mapRoutes(new UtilRoutes(), UtilRoutes.methods()),

        //api methods mongodb
        ...mapRoutes(new PhotoRoutes(mongoDbFile), PhotoRoutes.methods())
    ])

    await app.start()
    console.log('=> API SWAGGER OK na porta', app.info.port)
    console.log(`${'http://localhost:5000/documentation'}`)
    return app;
}

module.exports = main()