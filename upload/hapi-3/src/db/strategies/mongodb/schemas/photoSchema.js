const Mongoose = require('mongoose')

const PhotoSchema = new Mongoose.Schema({
    createdAt: {
        type: Date,
        default: Date.now
    }
})

//mocha workaround
module.exports = Mongoose.models.photo || Mongoose.model('photo', PhotoSchema)