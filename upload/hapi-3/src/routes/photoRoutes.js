const BaseRoute = require('./base/baseRoute')
const Joi = require('joi')
const fs = require('fs')
const {
    promisify
} = require('util')

const handleFileUpload = file => {
    return new Promise((resolve, reject) => {
        const filename = file.hapi.filename
        const data = file._data

        fs.writeFile(`./upload/${filename}`, data, err => {
            if (err) {
                reject(err)
            }
            resolve({
                message: 'Up ok!',
                imageUrl: `${server.info.uri}/upload/${filename}`
            })
        })
    })
}

class PhotoRoutes extends BaseRoute {
    constructor(db) {
        super()
        this.db = db
    }

    list() {
        return {
            path: '/photos',
            method: 'GET',
            config: {
                tags: ['api'],
                description: 'Lista os files ',
                notes: 'Retorna a base inteira de file',

                validate: {
                    failAction: (request, h, err) => {
                        throw err;
                    }
                }
            },
            handler: (request, headers) => {
                return this.db.read()
            }
        }
    }

    create() {
        return {
            path: '/photos',
            method: 'POST',
            config: {
                tags: ['api'],
                description: 'Cadastra novo file ',
                notes: 'add novo file',
                plugins: {
                    'hapi-swagger': {
                        payloadType: 'form'
                    }
                },
                validate: {
                    failAction: (request, h, err) => {
                        throw err;
                    },
                    payload: {
                        file: Joi.any()
                            .meta({
                                swaggerType: 'file'
                            })
                            .description('json file')
                    }
                },
                payload: {
                    maxBytes: 1048576,
                    parse: true,
                    output: 'stream'
                }
            },
            handler: (request, headers) => {
                const {
                    payload
                } = request

                const response = handleFileUpload(payload.file)
                return response
            }
        }
    }
}


module.exports = PhotoRoutes