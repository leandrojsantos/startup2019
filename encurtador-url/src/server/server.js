//para variaveis de ambiante e senhas
require('dotenv').config()

//express => faz requisoes http, o bodyparser e path o ajuda tranformando em api
const express = require('express')
const bodyParser = require('body-parser')
const path = require('path')

//dns => domínio está operacional passando a parte do nome do host da URL (o domínio) para dns.lookup que verifica se o domínio está dns.lookup . Nesse caso, podemos conectar-se à nossa instância do MongoDB e criar a versão abreviada do url
const dns = require('dns')

//conexao com mongodb
const {
  MongoClient
} = require('mongodb')
const databaseUrl = process.env.DATABASE

//nanoid => reduzir a URL para armazená-la no banco de dados, criando um ID curto e exclusivo para cada URL
const nanoid = require('nanoid')

const app = express()

app.use(bodyParser.urlencoded({
  extended: false
}))
app.use(bodyParser.json())

app.use(express.static(path.join(__dirname, 'public')))

MongoClient.connect(databaseUrl, {
    useUnifiedTopology: true,
    useNewUrlParser: true
  })
  .then(client => {
    app.locals.db = client.db('shortenedURLs')
  })
  .catch(() => console.error('==>ERRO ao CONECTAR MONGODB'))

//return shortenedURLs.findOneAndUpdate => para verificar se ja nao ja existe 
const shortenURL = (db, url) => {
  const shortenedURLs = db.collection('shortenedURLs')
  return shortenedURLs.findOneAndUpdate({
    originalUrl: url
  }, {
    $setOnInsert: {
      originalUrl: url,
      shortId: nanoid(7),
      //nanoid (7) => que é um ID exclusivo de 7 caracteres para essa URL.
    },
  }, {
    returnOriginal: false,
    upsert: true,
    //upsert=> caso ja esista essa URL no db sobrescrer para assim n gerar dublicidade
  })
}

//checkIfShortIdExists=> verifica se o id gerado pelo nanoid(7) ja n existe
const checkIfShortIdExists = (db, code) => db.collection('shortenedURLs')
  .findOne({
    shortId: code
  })


app.get('/', (resquest, res) => {
  const htmlPath = path.join(__dirname, 'public', 'index.html');
  res.sendFile(htmlPath);
})

app.post('/new', (resquest, res) => {
  //try catch com dns => para verificar se site existe
  let originalUrl
  try {
    originalUrl = new URL(resquest.body.url)
  } catch (err) {
    return res.status(400).send({
      error: 'deu merda 400'
    })
  }
  dns.lookup(originalUrl.hostname, (err) => {
    if (err) {
      return res.status(404).send({
        error: 'deu merda 404'
      })
    }

    const {
      db
    } = resquest.app.locals
    shortenURL(db, originalUrl.href)
      .then(result => {
        const doc = result.value
        res.json({
          originalUrl: doc.originalUrl,
          shortId: doc.shortId,
        })
      })
      .catch(console.error)
  })
})

app.get('/:shortId', (resquest, res) => {
  const shortId = resquest.params.shortId

  const {
    db
  } = resquest.app.locals
  checkIfShortIdExists(db, shortId)
    .then(doc => {
      if (doc === null) return res.send('DEU MERDA...sem dominio associado a esse id')

      res.redirect(doc.originalUrl)
    })
    .catch(console.error)

})

app.set('port', process.env.PORT || 4000)
const server = app.listen(app.get('port'), () => {
  console.log(`Api start → PORT ${server.address().port}`)

})